﻿namespace Receiver
{
    partial class ReceiverWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PortInput = new System.Windows.Forms.NumericUpDown();
            this.MetadataPortLabel = new System.Windows.Forms.Label();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.AddressInput = new System.Windows.Forms.TextBox();
            this.AddressLabel = new System.Windows.Forms.Label();
            this.ReceiveButton = new System.Windows.Forms.Button();
            this.StreamBox = new System.Windows.Forms.PictureBox();
            this.ReceiverWorker = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.PortInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StreamBox)).BeginInit();
            this.SuspendLayout();
            // 
            // PortInput
            // 
            this.PortInput.Location = new System.Drawing.Point(275, 287);
            this.PortInput.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.PortInput.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.PortInput.Name = "PortInput";
            this.PortInput.Size = new System.Drawing.Size(51, 20);
            this.PortInput.TabIndex = 30;
            this.PortInput.Value = new decimal(new int[] {
            15032,
            0,
            0,
            0});
            // 
            // MetadataPortLabel
            // 
            this.MetadataPortLabel.AutoSize = true;
            this.MetadataPortLabel.Location = new System.Drawing.Point(210, 289);
            this.MetadataPortLabel.Name = "MetadataPortLabel";
            this.MetadataPortLabel.Size = new System.Drawing.Size(59, 13);
            this.MetadataPortLabel.TabIndex = 29;
            this.MetadataPortLabel.Text = "Port (TCP):";
            // 
            // StatusLabel
            // 
            this.StatusLabel.AutoSize = true;
            this.StatusLabel.Location = new System.Drawing.Point(12, 320);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(71, 13);
            this.StatusLabel.TabIndex = 28;
            this.StatusLabel.Text = "Status: offline";
            // 
            // AddressInput
            // 
            this.AddressInput.Location = new System.Drawing.Point(64, 286);
            this.AddressInput.Name = "AddressInput";
            this.AddressInput.Size = new System.Drawing.Size(140, 20);
            this.AddressInput.TabIndex = 27;
            this.AddressInput.Text = "127.0.0.1";
            // 
            // AddressLabel
            // 
            this.AddressLabel.AutoSize = true;
            this.AddressLabel.Location = new System.Drawing.Point(10, 288);
            this.AddressLabel.Name = "AddressLabel";
            this.AddressLabel.Size = new System.Drawing.Size(48, 13);
            this.AddressLabel.TabIndex = 26;
            this.AddressLabel.Text = "Address:";
            // 
            // ReceiveButton
            // 
            this.ReceiveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReceiveButton.Location = new System.Drawing.Point(338, 286);
            this.ReceiveButton.Name = "ReceiveButton";
            this.ReceiveButton.Size = new System.Drawing.Size(154, 48);
            this.ReceiveButton.TabIndex = 25;
            this.ReceiveButton.Text = "▶";
            this.ReceiveButton.UseVisualStyleBackColor = true;
            this.ReceiveButton.Click += new System.EventHandler(this.ReceiveButton_Click);
            // 
            // StreamBox
            // 
            this.StreamBox.Location = new System.Drawing.Point(12, 12);
            this.StreamBox.Name = "StreamBox";
            this.StreamBox.Size = new System.Drawing.Size(480, 270);
            this.StreamBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.StreamBox.TabIndex = 24;
            this.StreamBox.TabStop = false;
            // 
            // ReceiverWorker
            // 
            this.ReceiverWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.ReceiverWorker_DoWork);
            // 
            // ReceiverWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(502, 342);
            this.Controls.Add(this.PortInput);
            this.Controls.Add(this.MetadataPortLabel);
            this.Controls.Add(this.StatusLabel);
            this.Controls.Add(this.AddressInput);
            this.Controls.Add(this.AddressLabel);
            this.Controls.Add(this.ReceiveButton);
            this.Controls.Add(this.StreamBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ReceiverWindow";
            this.Text = "JPEG-inator Receiver";
            ((System.ComponentModel.ISupportInitialize)(this.PortInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StreamBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown PortInput;
        private System.Windows.Forms.Label MetadataPortLabel;
        private System.Windows.Forms.Label StatusLabel;
        private System.Windows.Forms.TextBox AddressInput;
        private System.Windows.Forms.Label AddressLabel;
        private System.Windows.Forms.Button ReceiveButton;
        private System.Windows.Forms.PictureBox StreamBox;
        private System.ComponentModel.BackgroundWorker ReceiverWorker;
    }
}

