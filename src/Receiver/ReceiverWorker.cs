﻿using System.Drawing;
using System.IO;
using Core;

namespace Receiver
{
    public partial class ReceiverWindow
    {
        private void ReceiverWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            TcpHandler tcp = new TcpHandler(_address, _port);
            StatusLabel.Text = "Status: connected, waiting for stream";
            // Keep receiving new frames until the user stops
            while (_isReceiving)
            {
                // Get size of frame and frame number
                if (int.TryParse(tcp.ReceiveText(10), out int length))
                {
                    StatusLabel.Text = $"Status: receiving frame that's {length} bytes big";
                    // Get image if arrived correctly
                    using (MemoryStream image = new MemoryStream(tcp.Receive(length)))
                    {
                        _currentFrame = Image.FromStream(image);
                        StreamBox.Image = _currentFrame;
                        // Garbage collection
                        _oldFrameReference?.Dispose();
                        _oldFrameReference = _currentFrame;
                    }
                }
                else
                {
                    // If the length couldn't be determined, aka if an error occured, reset the stream
                    StatusLabel.Text = $"Status: received corrupted frame";
                    tcp.Dispose();
                    tcp = new TcpHandler(_address, _port);
                }
            }
            tcp.Dispose();
        }
    }
}
