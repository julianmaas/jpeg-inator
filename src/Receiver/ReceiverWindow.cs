﻿using System.Drawing;
using System.Windows.Forms;

namespace Receiver
{
    public partial class ReceiverWindow : Form
    {
        // Image variables
        private Image _currentFrame; // Current frame
        private Image _oldFrameReference; // Reference to current frame so it can be garbage collected

        // Server variables
        private string _address; // Address to broadcast to
        private ushort _port; // Port for connection
        private bool _isReceiving;

        public ReceiverWindow()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
        }

        private void ReceiveButton_Click(object sender, System.EventArgs e)
        {
            if (!_isReceiving)
            {
                // Disables network settings adjustment
                AddressInput.Enabled = false;
                PortInput.Enabled = false;

                // Set correct connection details
                _address = AddressInput.Text;
                _port = (ushort) PortInput.Value;

                // Start receiving
                _isReceiving = true;
                ReceiverWorker.RunWorkerAsync();

                // Show broadcasting status
                ReceiveButton.Text = "■";
                StatusLabel.Text = "Status: connecting";
            }
            else
            {
                // Stops broadcasting
                _isReceiving = false;
                ReceiveButton.Text = "▶";
                StatusLabel.Text = "Status: offline";

                // Enables settings adjustment
                AddressInput.Enabled = true;
                PortInput.Enabled = true;
            }
        }
    }
}
