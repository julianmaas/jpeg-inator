﻿using System.IO;
using System.Threading;
using Core;

namespace Server
{
    public partial class ServerWindow
    {
        private void SendBuffer(BufferedTcpHandler client)
        {
            while (true)
            {
                if (client.Buffer.BufferSize != 0)
                {
                    try
                    {
                        byte[] frame = client.Buffer.GetFromBuffer();
                        // Send size of image
                        client.Send(TcpHandler.GetLengthToTransmit(frame.Length));
                        // Send image
                        client.Send(new MemoryStream(frame));
                    }
                    catch
                    {
                        // Check if client is still online; discard client if offline and stop this method
                        if (!client.IsOnline())
                        {
                            client.Dispose();
                            _receivers.Remove(client);
                            break;
                        }
                    }
                }
                else
                    Thread.Sleep(1);
            }
        }
    }
}
