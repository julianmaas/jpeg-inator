﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using Core;

namespace Server
{
    public partial class ServerWindow : Form
    {
        // Stores frames
        private Image _currentFrame; // Current frame for preview purposes
        private Image _oldFrameReference; // Reference to current frame so it can be garbage collected

        private List<BufferedTcpHandler> _receivers = new List<BufferedTcpHandler>(); // Receivers of the broadcast
        private TcpHandler _server; // Receives broadcast from broadcaster
        private bool _isListening = false; // Determines if the application is listening
       
        private bool _isFpsGoalReached = true; // Determines if the FPS goal is being reached
        private ushort _framesReceived = 0; // Frames received within a second

        public ServerWindow()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
            FpsWorker.RunWorkerAsync();
        }

        /// <summary>
        /// Makes sure application is closed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void ServerWindow_Closing(object sender, EventArgs eventArgs)
        {
            Application.Exit();
        }

        /// <summary>
        /// Starts or stops listening for connection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StartServerButton_Click(object sender, System.EventArgs e)
        {
            if (!_isListening && !GetBroadcastWorker.IsBusy)
            {
                // Disables network settings adjustment
                PortInput.Enabled = false;
                PublicPortInput.Enabled = false;

                // Start waiting for connection
                _isListening = true;
                GetBroadcastWorker.RunWorkerAsync(PortInput.Value);
                GetReceiverWorker.RunWorkerAsync(PublicPortInput.Value);

                // Show broadcasting status
                StartServerButton.Text = "■";
                StatusLabel.Text = "Status: waiting for broadcast";
            }
            else
            {
                // Stops broadcasting
                _isListening = false;

                // Restarts
                Application.Restart();
                Environment.Exit(0);
            }
        }

        /// <summary>
        /// Checks if fps goal is reached
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FpsWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            while (true)
            {
                // Wait a second
                Thread.Sleep(1000);
                // If a broadcast is being received and doesn't meet the fps criteria
                if (_isListening && _framesReceived < FpsInput.Value)
                    _isFpsGoalReached = false;
                else
                    _isFpsGoalReached = true;
                
                // Reset fps
                _framesReceived = 0;
            }
        }
    }
}
