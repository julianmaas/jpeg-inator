﻿namespace Server
{
    partial class ServerWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CameraPreviewBox = new System.Windows.Forms.PictureBox();
            this.GetBroadcastWorker = new System.ComponentModel.BackgroundWorker();
            this.PortInput = new System.Windows.Forms.NumericUpDown();
            this.PortLabel = new System.Windows.Forms.Label();
            this.Tabs = new System.Windows.Forms.TabControl();
            this.PreviewTab = new System.Windows.Forms.TabPage();
            this.ConsoleTab = new System.Windows.Forms.TabPage();
            this.ConsoleBox = new System.Windows.Forms.RichTextBox();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.StartServerButton = new System.Windows.Forms.Button();
            this.PublicPortLabel = new System.Windows.Forms.Label();
            this.PublicPortInput = new System.Windows.Forms.NumericUpDown();
            this.GetReceiverWorker = new System.ComponentModel.BackgroundWorker();
            this.FpsWorker = new System.ComponentModel.BackgroundWorker();
            this.FpsLabel = new System.Windows.Forms.Label();
            this.FpsInput = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.CameraPreviewBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PortInput)).BeginInit();
            this.Tabs.SuspendLayout();
            this.PreviewTab.SuspendLayout();
            this.ConsoleTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PublicPortInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FpsInput)).BeginInit();
            this.SuspendLayout();
            // 
            // CameraPreviewBox
            // 
            this.CameraPreviewBox.Location = new System.Drawing.Point(6, 3);
            this.CameraPreviewBox.Name = "CameraPreviewBox";
            this.CameraPreviewBox.Size = new System.Drawing.Size(480, 270);
            this.CameraPreviewBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.CameraPreviewBox.TabIndex = 3;
            this.CameraPreviewBox.TabStop = false;
            // 
            // GetBroadcastWorker
            // 
            this.GetBroadcastWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.GetBroadcastWorker_DoWork);
            // 
            // PortInput
            // 
            this.PortInput.Location = new System.Drawing.Point(144, 311);
            this.PortInput.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.PortInput.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.PortInput.Name = "PortInput";
            this.PortInput.Size = new System.Drawing.Size(51, 20);
            this.PortInput.TabIndex = 27;
            this.PortInput.Value = new decimal(new int[] {
            15031,
            0,
            0,
            0});
            // 
            // PortLabel
            // 
            this.PortLabel.AutoSize = true;
            this.PortLabel.Location = new System.Drawing.Point(12, 314);
            this.PortLabel.Name = "PortLabel";
            this.PortLabel.Size = new System.Drawing.Size(126, 13);
            this.PortLabel.TabIndex = 26;
            this.PortLabel.Text = "Broadcaster\'s Port (TCP):";
            // 
            // Tabs
            // 
            this.Tabs.Controls.Add(this.PreviewTab);
            this.Tabs.Controls.Add(this.ConsoleTab);
            this.Tabs.Location = new System.Drawing.Point(2, 3);
            this.Tabs.Name = "Tabs";
            this.Tabs.SelectedIndex = 0;
            this.Tabs.Size = new System.Drawing.Size(501, 308);
            this.Tabs.TabIndex = 28;
            // 
            // PreviewTab
            // 
            this.PreviewTab.Controls.Add(this.CameraPreviewBox);
            this.PreviewTab.Location = new System.Drawing.Point(4, 22);
            this.PreviewTab.Name = "PreviewTab";
            this.PreviewTab.Padding = new System.Windows.Forms.Padding(3);
            this.PreviewTab.Size = new System.Drawing.Size(493, 282);
            this.PreviewTab.TabIndex = 1;
            this.PreviewTab.Text = "Preview";
            this.PreviewTab.UseVisualStyleBackColor = true;
            // 
            // ConsoleTab
            // 
            this.ConsoleTab.Controls.Add(this.ConsoleBox);
            this.ConsoleTab.Location = new System.Drawing.Point(4, 22);
            this.ConsoleTab.Name = "ConsoleTab";
            this.ConsoleTab.Padding = new System.Windows.Forms.Padding(3);
            this.ConsoleTab.Size = new System.Drawing.Size(493, 282);
            this.ConsoleTab.TabIndex = 0;
            this.ConsoleTab.Text = "Console";
            this.ConsoleTab.UseVisualStyleBackColor = true;
            // 
            // ConsoleBox
            // 
            this.ConsoleBox.Enabled = false;
            this.ConsoleBox.Location = new System.Drawing.Point(6, 6);
            this.ConsoleBox.Name = "ConsoleBox";
            this.ConsoleBox.Size = new System.Drawing.Size(481, 270);
            this.ConsoleBox.TabIndex = 0;
            this.ConsoleBox.Text = "";
            // 
            // StatusLabel
            // 
            this.StatusLabel.AutoSize = true;
            this.StatusLabel.Location = new System.Drawing.Point(201, 314);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(71, 13);
            this.StatusLabel.TabIndex = 29;
            this.StatusLabel.Text = "Status: offline";
            // 
            // StartServerButton
            // 
            this.StartServerButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StartServerButton.Location = new System.Drawing.Point(328, 337);
            this.StartServerButton.Name = "StartServerButton";
            this.StartServerButton.Size = new System.Drawing.Size(164, 20);
            this.StartServerButton.TabIndex = 30;
            this.StartServerButton.Text = "▶";
            this.StartServerButton.UseVisualStyleBackColor = true;
            this.StartServerButton.Click += new System.EventHandler(this.StartServerButton_Click);
            // 
            // PublicPortLabel
            // 
            this.PublicPortLabel.AutoSize = true;
            this.PublicPortLabel.Location = new System.Drawing.Point(12, 338);
            this.PublicPortLabel.Name = "PublicPortLabel";
            this.PublicPortLabel.Size = new System.Drawing.Size(98, 13);
            this.PublicPortLabel.TabIndex = 31;
            this.PublicPortLabel.Text = "Public\'s Port (TCP):";
            // 
            // PublicPortInput
            // 
            this.PublicPortInput.Location = new System.Drawing.Point(144, 337);
            this.PublicPortInput.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.PublicPortInput.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.PublicPortInput.Name = "PublicPortInput";
            this.PublicPortInput.Size = new System.Drawing.Size(51, 20);
            this.PublicPortInput.TabIndex = 32;
            this.PublicPortInput.Value = new decimal(new int[] {
            15032,
            0,
            0,
            0});
            // 
            // GetReceiverWorker
            // 
            this.GetReceiverWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.GetReceiverWorker_DoWork);
            // 
            // FpsWorker
            // 
            this.FpsWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.FpsWorker_DoWork);
            // 
            // FpsLabel
            // 
            this.FpsLabel.AutoSize = true;
            this.FpsLabel.Location = new System.Drawing.Point(201, 339);
            this.FpsLabel.Name = "FpsLabel";
            this.FpsLabel.Size = new System.Drawing.Size(74, 13);
            this.FpsLabel.TabIndex = 33;
            this.FpsLabel.Text = "Minimum FPS:";
            // 
            // FpsInput
            // 
            this.FpsInput.Location = new System.Drawing.Point(281, 336);
            this.FpsInput.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.FpsInput.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.FpsInput.Name = "FpsInput";
            this.FpsInput.Size = new System.Drawing.Size(41, 20);
            this.FpsInput.TabIndex = 34;
            this.FpsInput.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // ServerWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(505, 363);
            this.Controls.Add(this.FpsInput);
            this.Controls.Add(this.FpsLabel);
            this.Controls.Add(this.PublicPortInput);
            this.Controls.Add(this.PublicPortLabel);
            this.Controls.Add(this.StartServerButton);
            this.Controls.Add(this.StatusLabel);
            this.Controls.Add(this.Tabs);
            this.Controls.Add(this.PortInput);
            this.Controls.Add(this.PortLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ServerWindow";
            this.Text = "JPEG-inator Server";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.ServerWindow_Closing);
            ((System.ComponentModel.ISupportInitialize)(this.CameraPreviewBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PortInput)).EndInit();
            this.Tabs.ResumeLayout(false);
            this.PreviewTab.ResumeLayout(false);
            this.ConsoleTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PublicPortInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FpsInput)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox CameraPreviewBox;
        private System.ComponentModel.BackgroundWorker GetBroadcastWorker;
        private System.Windows.Forms.NumericUpDown PortInput;
        private System.Windows.Forms.Label PortLabel;
        private System.Windows.Forms.TabControl Tabs;
        private System.Windows.Forms.TabPage PreviewTab;
        private System.Windows.Forms.TabPage ConsoleTab;
        private System.Windows.Forms.RichTextBox ConsoleBox;
        private System.Windows.Forms.Label StatusLabel;
        private System.Windows.Forms.Button StartServerButton;
        private System.Windows.Forms.Label PublicPortLabel;
        private System.Windows.Forms.NumericUpDown PublicPortInput;
        private System.ComponentModel.BackgroundWorker GetReceiverWorker;
        private System.ComponentModel.BackgroundWorker FpsWorker;
        private System.Windows.Forms.Label FpsLabel;
        private System.Windows.Forms.NumericUpDown FpsInput;
    }
}

