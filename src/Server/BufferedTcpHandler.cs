﻿using System.Net.Sockets;
using Core;

namespace Server
{
    /// <summary>
    /// TcpHandler with built in buffer class
    /// </summary>
    public class BufferedTcpHandler : TcpHandler
    {
        public Buffer Buffer { get; } = new Buffer();

        public BufferedTcpHandler(string address, ushort port) : base(address, port)
        {
        }

        public BufferedTcpHandler(ushort port) : base(port)
        {
        }

        public BufferedTcpHandler(TcpClient client) : base(client)
        {
        }
    }
}
