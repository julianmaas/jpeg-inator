﻿using System.ComponentModel;
using System.Drawing;
using System.IO;
using Core;

namespace Server
{
    public partial class ServerWindow
    {
        /// <summary>
        /// Gets frames to show
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GetBroadcastWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            // Get connection from broadcaster
            _server = new TcpHandler((ushort) (decimal) e.Argument);
            StatusLabel.Text = "Status: broadcaster connected";
            while (_isListening)
            {
                // Step 1: Determine image size (Determined by first 10 bytes)
                if (int.TryParse(_server.ReceiveText(10), out int length))
                {
                    ConsoleBox.Text = $"Receiving frame that's {length} bytes big";
                    // Step 2: Get image
                    using (MemoryStream image = new MemoryStream(_server.Receive(length)))
                    {
                        // Shows frame preview to user
                        _currentFrame = Image.FromStream(image);
                        CameraPreviewBox.Image = _currentFrame;
                        // Gets rid of previous frame and marks current frame as previous frame (garbage collection)
                        _oldFrameReference?.Dispose();
                        _oldFrameReference = _currentFrame;

                        // Increase frames received this second
                        _framesReceived++;
                        
                        // Show how many clients are connected
                        ConsoleBox.Text += $"\nSending frame to {_receivers.Count} ";
                        if (_receivers.Count == 1)
                            ConsoleBox.Text += "client";
                        else
                            ConsoleBox.Text += "clients";

                        // Store new frame in the buffer of all connected receivers
                        byte[] imageData = image.ToArray();
                        foreach (BufferedTcpHandler receiver in _receivers)
                            receiver.Buffer.AddToBuffer(imageData);
                    }
                }
                else
                {
                    // If the length couldn't be determined, aka if an error occured, reset the stream
                    ConsoleBox.Text = $"Receiving corrupted frame";
                }
                // Flush to keep stream running smoothly
                _server.Flush();
                // Step 4: Acknowledge that the frame arrived by telling the broadcaster if it should lower its quality or not
                _server.Send(_isFpsGoalReached ? "k" : "l");
                // k: Fps goal reached: broadcaster can increase quality or keep max quality
                // l: Fps goal not reached: broadcaster should lower quality
            }
        }
    }
}
