﻿using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Server
{
    public partial class ServerWindow
    {
        /// <summary>
        /// Add new clients
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GetReceiverWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            TcpListener listener = new TcpListener(IPAddress.Any, (ushort)(decimal)e.Argument);
            listener.Start();
            // Only add new clients if the application is in listening mode
            while (_isListening)
            {
                BufferedTcpHandler newClient = new BufferedTcpHandler(listener.AcceptTcpClient());
                _receivers.Add(newClient);
                // Start buffering to client
                new Thread(() => SendBuffer(newClient)).Start();
            }
                
            listener.Stop();
        }
    }
}
