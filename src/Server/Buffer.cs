﻿using System.Collections.Generic;

namespace Server
{
    /// <summary>
    /// Buffer implementation
    /// </summary>
    public class Buffer
    {
        private LinkedList<byte[]> _frames = new LinkedList<byte[]>();

        // Least recent frames get removed after max buffer size has been reached
        public int MaxBufferSize { get; set; } = 100;
        public int BufferSize => _frames.Count;

        /// <summary>
        /// Adds frame to buffer and disposes least recent frame if buffer is full
        /// </summary>
        /// <param name="data"></param>
        public void AddToBuffer(byte[] data)
        {
            if (_frames.Count >= MaxBufferSize)
                _frames.RemoveFirst();
            _frames.AddLast(data);
        }

        /// <summary>
        /// Get the least recent frame and consume it
        /// </summary>
        /// <returns></returns>
        public byte[] GetFromBuffer()
        {
            byte[] data = _frames.First.Value;
            _frames.RemoveFirst();
            return data;
        }
    }
}
