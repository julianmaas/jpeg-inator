﻿using System.ComponentModel;
using System.Drawing;
using System.IO;
using Core;

namespace Broadcaster
{
    public partial class BroadcasterWindow
    {
        /// <summary>
        /// Encodes frame
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EncodingWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                Bitmap frame = (Bitmap) e.Argument;
                // Resize if requested
                if (_size < 1.0m)
                    frame = _imageEncoder.Resize(frame, (int)(frame.Width * _size), (int)(frame.Height * _size));
                
                // Encodes frame to JPEG format
                _imageEncoder.Encode(frame, stream, _quality);
                _currentFrame = Image.FromStream(stream);

                // Show frame
                CameraPreviewBox.Image = _currentFrame;

                // Broadcast frame
                if (_isBroadcasting)
                {
                    // Check if a broadcaster exists, if not connect
                    if (_tcp == null)
                    {
                        _tcp = new TcpHandler(_address, _port);
                        StatusLabel.Text = "Status: broadcasting";
                    }

                    // Makes sure server is online
                    if (!_tcp.IsOnline()) 
                        StopBroadcastingMode();

                    // Sends the length of the image to be sent (10 bytes)
                    _tcp?.Send(TcpHandler.GetLengthToTransmit(stream.Length)); 
                    // Sends image
                    _tcp?.Send(stream);
                    
                    // Get quality status of server as acknowledgement
                    char? status =_tcp?.ReceiveText(1)[0];

                    // Server asks the broadcaster to lower quality
                    if (status == 'l' && _quality > 1)
                        _quality--;
                    // Server acknowledges that the broadcaster meets its FPS goal
                    if (status == 'k' && _quality < 100)
                        _quality++;
                }
            }

            // Gets rid of previous frame and marks current frame as previous frame (garbage collection)
            _oldFrameReference?.Dispose();
            _oldFrameReference = _currentFrame;
        }
    }
}
