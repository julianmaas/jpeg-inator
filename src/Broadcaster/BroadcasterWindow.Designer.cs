﻿using System.ComponentModel;

namespace Broadcaster
{
    partial class BroadcasterWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CameraLabel = new System.Windows.Forms.Label();
            this.CameraSelectionBox = new System.Windows.Forms.ComboBox();
            this.CameraPreviewBox = new System.Windows.Forms.PictureBox();
            this.BroadcastButton = new System.Windows.Forms.Button();
            this.AddressLabel = new System.Windows.Forms.Label();
            this.AddressInput = new System.Windows.Forms.TextBox();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.EncodingWorker = new System.ComponentModel.BackgroundWorker();
            this.SizeBar = new System.Windows.Forms.TrackBar();
            this.SizeLabel = new System.Windows.Forms.Label();
            this.MetadataPortLabel = new System.Windows.Forms.Label();
            this.PortInput = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.CameraPreviewBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SizeBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PortInput)).BeginInit();
            this.SuspendLayout();
            // 
            // CameraLabel
            // 
            this.CameraLabel.AutoSize = true;
            this.CameraLabel.Location = new System.Drawing.Point(12, 9);
            this.CameraLabel.Name = "CameraLabel";
            this.CameraLabel.Size = new System.Drawing.Size(49, 13);
            this.CameraLabel.TabIndex = 0;
            this.CameraLabel.Text = "Camera: ";
            // 
            // CameraSelectionBox
            // 
            this.CameraSelectionBox.DisplayMember = "Name";
            this.CameraSelectionBox.FormattingEnabled = true;
            this.CameraSelectionBox.Location = new System.Drawing.Point(67, 6);
            this.CameraSelectionBox.Name = "CameraSelectionBox";
            this.CameraSelectionBox.Size = new System.Drawing.Size(428, 21);
            this.CameraSelectionBox.TabIndex = 1;
            this.CameraSelectionBox.Text = "Select a camera";
            this.CameraSelectionBox.ValueMember = "Name";
            this.CameraSelectionBox.SelectedIndexChanged += new System.EventHandler(this.CameraSelectionBox_SelectedIndexChanged);
            // 
            // CameraPreviewBox
            // 
            this.CameraPreviewBox.Location = new System.Drawing.Point(15, 33);
            this.CameraPreviewBox.Name = "CameraPreviewBox";
            this.CameraPreviewBox.Size = new System.Drawing.Size(480, 270);
            this.CameraPreviewBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.CameraPreviewBox.TabIndex = 2;
            this.CameraPreviewBox.TabStop = false;
            // 
            // BroadcastButton
            // 
            this.BroadcastButton.Enabled = false;
            this.BroadcastButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BroadcastButton.Location = new System.Drawing.Point(335, 310);
            this.BroadcastButton.Name = "BroadcastButton";
            this.BroadcastButton.Size = new System.Drawing.Size(154, 48);
            this.BroadcastButton.TabIndex = 3;
            this.BroadcastButton.Text = "▶";
            this.BroadcastButton.UseVisualStyleBackColor = true;
            this.BroadcastButton.Click += new System.EventHandler(this.BroadcastButton_Click);
            // 
            // AddressLabel
            // 
            this.AddressLabel.AutoSize = true;
            this.AddressLabel.Location = new System.Drawing.Point(13, 309);
            this.AddressLabel.Name = "AddressLabel";
            this.AddressLabel.Size = new System.Drawing.Size(48, 13);
            this.AddressLabel.TabIndex = 4;
            this.AddressLabel.Text = "Address:";
            // 
            // AddressInput
            // 
            this.AddressInput.Location = new System.Drawing.Point(67, 307);
            this.AddressInput.Name = "AddressInput";
            this.AddressInput.Size = new System.Drawing.Size(140, 20);
            this.AddressInput.TabIndex = 5;
            this.AddressInput.Text = "127.0.0.1";
            // 
            // StatusLabel
            // 
            this.StatusLabel.AutoSize = true;
            this.StatusLabel.Location = new System.Drawing.Point(188, 337);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(141, 13);
            this.StatusLabel.TabIndex = 17;
            this.StatusLabel.Text = "Status: no webcam selected";
            // 
            // EncodingWorker
            // 
            this.EncodingWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.EncodingWorker_DoWork);
            // 
            // SizeBar
            // 
            this.SizeBar.Location = new System.Drawing.Point(78, 333);
            this.SizeBar.Maximum = 100;
            this.SizeBar.Minimum = 1;
            this.SizeBar.Name = "SizeBar";
            this.SizeBar.Size = new System.Drawing.Size(104, 45);
            this.SizeBar.TabIndex = 20;
            this.SizeBar.TickFrequency = 10;
            this.SizeBar.Value = 100;
            this.SizeBar.Scroll += new System.EventHandler(this.SizeBar_Scroll);
            // 
            // SizeLabel
            // 
            this.SizeLabel.AutoSize = true;
            this.SizeLabel.Location = new System.Drawing.Point(13, 337);
            this.SizeLabel.Name = "SizeLabel";
            this.SizeLabel.Size = new System.Drawing.Size(59, 13);
            this.SizeLabel.TabIndex = 21;
            this.SizeLabel.Text = "Size: 100%";
            // 
            // MetadataPortLabel
            // 
            this.MetadataPortLabel.AutoSize = true;
            this.MetadataPortLabel.Location = new System.Drawing.Point(213, 310);
            this.MetadataPortLabel.Name = "MetadataPortLabel";
            this.MetadataPortLabel.Size = new System.Drawing.Size(59, 13);
            this.MetadataPortLabel.TabIndex = 22;
            this.MetadataPortLabel.Text = "Port (TCP):";
            // 
            // PortInput
            // 
            this.PortInput.Location = new System.Drawing.Point(278, 308);
            this.PortInput.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.PortInput.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.PortInput.Name = "PortInput";
            this.PortInput.Size = new System.Drawing.Size(51, 20);
            this.PortInput.TabIndex = 23;
            this.PortInput.Value = new decimal(new int[] {
            15031,
            0,
            0,
            0});
            // 
            // BroadcasterWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(501, 362);
            this.Controls.Add(this.PortInput);
            this.Controls.Add(this.MetadataPortLabel);
            this.Controls.Add(this.SizeLabel);
            this.Controls.Add(this.SizeBar);
            this.Controls.Add(this.StatusLabel);
            this.Controls.Add(this.AddressInput);
            this.Controls.Add(this.AddressLabel);
            this.Controls.Add(this.BroadcastButton);
            this.Controls.Add(this.CameraPreviewBox);
            this.Controls.Add(this.CameraSelectionBox);
            this.Controls.Add(this.CameraLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "BroadcasterWindow";
            this.Text = "JPEG-inator Broadcaster";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.BroadcasterWindow_Closing);
            this.Load += new System.EventHandler(this.BroadcasterWindow_Load);
            ((System.ComponentModel.ISupportInitialize)(this.CameraPreviewBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SizeBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PortInput)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label CameraLabel;
        private System.Windows.Forms.ComboBox CameraSelectionBox;
        private System.Windows.Forms.PictureBox CameraPreviewBox;
        private System.Windows.Forms.Button BroadcastButton;
        private System.Windows.Forms.Label AddressLabel;
        private System.Windows.Forms.TextBox AddressInput;
        private System.Windows.Forms.Label StatusLabel;
        private BackgroundWorker EncodingWorker;
        private System.Windows.Forms.TrackBar SizeBar;
        private System.Windows.Forms.Label SizeLabel;
        private System.Windows.Forms.Label MetadataPortLabel;
        private System.Windows.Forms.NumericUpDown PortInput;
    }
}

