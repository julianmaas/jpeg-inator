﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using AForge.Video;
using AForge.Video.DirectShow;
using Core;

namespace Broadcaster
{
    public partial class BroadcasterWindow : Form
    {
        // Image variables
        private ImageEncoder _imageEncoder = new ImageEncoder(ImageFormat.Jpeg); // Encoder for images
        private VideoCaptureDevice _captureDevice; // Active webcam
        private Image _currentFrame; // Current frame
        private Image _oldFrameReference; // Reference to current frame so it can be garbage collected

        // Image quality
        private int _quality = 100; // Image quality
        private decimal _size = 1.0m; // Image resizer
        
        // Server variables
        private TcpHandler _tcp; // Broadcasts
        private string _address; // Address to broadcast to
        private ushort _port; // Port for connection
        private bool _isBroadcasting = false; // Determines if the application is broadcasting

        public BroadcasterWindow()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
        }

        /// <summary>
        /// Gets all webcams
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BroadcasterWindow_Load(object sender, EventArgs e)
        {
            // Get and list webcams
            foreach (FilterInfo filterInfo in new FilterInfoCollection(FilterCategory.VideoInputDevice))
                CameraSelectionBox.Items.Add(filterInfo);
        }

        /// <summary>
        /// Stop using webcam when app closes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void BroadcasterWindow_Closing(object sender, EventArgs eventArgs)
        {
            if (_captureDevice?.IsRunning == true)
                _captureDevice.Stop();
        }

        /// <summary>
        /// Updates size
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SizeBar_Scroll(object sender, EventArgs e)
        {
            _size = SizeBar.Value/100.0m;
            SizeLabel.Text = $"Size: {(int)(_size*100)}%";
        }

        /// <summary>
        /// Changes webcam
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CameraSelectionBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Stops previously selected webcam
            if (_captureDevice?.IsRunning == true)
                _captureDevice.Stop();

            // Start selected webcam
            FilterInfo info = (FilterInfo) CameraSelectionBox.SelectedItem;
            _captureDevice = new VideoCaptureDevice(info.MonikerString);
            _captureDevice.NewFrame += Video_NewFrame;
            _captureDevice.Start();
            StatusLabel.Text = "Status: webcam changed";

            // Allow broadcasting
            BroadcastButton.Enabled = true;
        }

        /// <summary>
        /// New frame event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void Video_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            // Only process new frame if the other frame is done processing to prevent delay
            if (!EncodingWorker.IsBusy)
                EncodingWorker.RunWorkerAsync(new Bitmap(eventArgs.Frame));
        }

        /// <summary>
        /// Starts or stops broadcasting webcam
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BroadcastButton_Click(object sender, EventArgs e)
        {
            if (!_isBroadcasting)
            {
                // Disables network settings adjustment
                AddressInput.Enabled = false;
                PortInput.Enabled = false;

                // Set correct connection details
                _address = AddressInput.Text;
                _port = (ushort) PortInput.Value;

                // Start broadcasting
                _isBroadcasting = true;

                // Show broadcasting status
                BroadcastButton.Text = "■";
                StatusLabel.Text = "Status: connecting";
            }
            else
                StopBroadcastingMode();
        }

        /// <summary>
        /// Puts application in a state where it seizes broadcasting
        /// </summary>
        private void StopBroadcastingMode()
        {
            // Stops broadcasting
            _tcp.Dispose();
            _tcp = null;
            _isBroadcasting = false;
            BroadcastButton.Text = "▶";
            StatusLabel.Text = "Status: offline";

            // Enables settings adjustment
            AddressInput.Enabled = true;
            PortInput.Enabled = true;
        }
    }
}
