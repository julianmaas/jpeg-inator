﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

namespace Broadcaster
{
    class ImageEncoder
    {
        private ImageCodecInfo _imageCodecInfo;

        public ImageEncoder(ImageFormat format)
        {
            _imageCodecInfo = GetEncoder(format);
        }

        /// <summary>
        /// Resizes image
        /// </summary>
        /// <param name="image"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        public Bitmap Resize(Image image, int width, int height)
        {
            Bitmap resizedBitmap = new Bitmap(width, height);
            using (Graphics graphics = Graphics.FromImage(resizedBitmap))
            {
                graphics.InterpolationMode = InterpolationMode.Low;
                graphics.DrawImage(image, new Rectangle(0, 0, width, height), new Rectangle(0, 0, image.Width, image.Height), GraphicsUnit.Pixel);
            }
            // Garbage collection
            image.Dispose();

            return resizedBitmap;
        }

        /// <summary>
        /// Converts image to selected codec and quality
        /// </summary>
        /// <param name="input"></param>
        /// <param name="output"></param>
        /// <param name="quality"></param>
        public void Encode(Image input, Stream output, long quality)
        {
            // Sets quality level
            EncoderParameters parameter = new EncoderParameters(1)
            {
                Param = {[0] = new EncoderParameter(Encoder.Quality, quality)}
            };
            input.Save(output, _imageCodecInfo, parameter);
        }

        /// <summary>
        /// Gets correct codec
        /// </summary>
        /// <param name="format"></param>
        /// <returns></returns>
        private static ImageCodecInfo GetEncoder(ImageFormat format)
        {
            foreach (ImageCodecInfo codec in ImageCodecInfo.GetImageDecoders())
                if (codec.FormatID == format.Guid)
                    return codec;
            
            throw new ArgumentException("Codec not found");
        }
    }
}
