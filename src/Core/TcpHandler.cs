﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Core
{
    /// <summary>
    /// Wrapper around a TCP client in order keep networking code clean
    /// </summary>
    public class TcpHandler : IDisposable
    {
        private TcpClient _client;
        public Encoding Encoding { get; set; } = Encoding.ASCII;

        /// <summary>
        /// Constructor for clients to connect to a server
        /// </summary>
        /// <param name="address"></param>
        /// <param name="port"></param>
        public TcpHandler(string address, ushort port)
        {
            _client = new TcpClient();
            _client.Connect(address, port);
        }

        /// <summary>
        /// Constructor for a server to get a client
        /// </summary>
        /// <param name="port"></param>
        public TcpHandler(ushort port)
        {
            TcpListener listener = new TcpListener(IPAddress.Any, port);
            listener.Start();
            _client = listener.AcceptTcpClient();
        }

        /// <summary>
        /// Constructor to wrap an already existing TCP client in
        /// </summary>
        /// <param name="client"></param>
        public TcpHandler(TcpClient client)
        {
            _client = client;
        }
        
        /// <summary>
        /// Checks if client is online
        /// </summary>
        /// <returns></returns>
        public bool IsOnline() => _client.Connected;

        /// <summary>
        /// Sends data
        /// </summary>
        /// <param name="stream"></param>
        public void Send(MemoryStream stream)
        {
            byte[] bytes = stream.ToArray();
            _client.GetStream().Write(bytes, 0, bytes.Length);
        }

        /// <summary>
        /// Sends text
        /// </summary>
        /// <param name="text"></param>
        public void Send(string text)
        {
            byte[] bytes = Encoding.GetBytes(text);
            _client.GetStream().Write(bytes, 0 , bytes.Length);
        }

        /// <summary>
        /// Receives data
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public byte[] Receive(int length)
        {
            byte[] bytes = new byte[length];
            // Makes sure all bytes are read and reads more bytes if that's not the case
            int totalBytesRead = 0;
            do
                totalBytesRead += _client.GetStream().Read(bytes, totalBytesRead, length-totalBytesRead);
            while (totalBytesRead < length);
            
            return bytes;
        }

        /// <summary>
        /// Receives text
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public string ReceiveText(int length)
        {
            return Encoding.GetString(Receive(length));
        }

        /// <summary>
        /// Clears everything in the stream in case of an error
        /// </summary>
        public void Flush()
        {
            while (_client.GetStream().DataAvailable)
                _client.GetStream().ReadByte();
        }

        /// <summary>
        /// Disposable implementation
        /// </summary>
        public void Dispose()
        {
            _client.Dispose();
        }

        /// <summary>
        /// Gets length in string form
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string GetLengthToTransmit(long length)
        {
            string lengthText = $"{length}";
            while (lengthText.Length < 10)
                lengthText = $"0{lengthText}";
            return lengthText;
        }
    }
}
