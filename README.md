## JPEG-inator

Compresses a webcam stream as a series of JPEGs and then sends the stream to a server that resends it to multiple clients.\
[Full article: https://julianmaas.com/custom-streaming-protocol/](https://julianmaas.com/custom-streaming-protocol/)

###### Warning: Pretty buggy in its current state!